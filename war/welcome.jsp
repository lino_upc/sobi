<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>WherEat</title>
    <c:choose>
        <c:when test="${showRawData}">
        </head>
        <body>
        <pre>
<c:out value="${places}"/>
        </pre>
        </body>
        </c:when>
        <c:otherwise>
		    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		    <style type="text/css">
		      html { height: 100% }
		      body { height: 100%; margin: 0; padding: 0; font-family:"Segoe UI"; font-size:14px; }
		      p {font-weight:bold;}
		      #side_bar {  float:left; width:100px; height:100%; border-right:1px solid black; padding:100px 5px 50px 5px;}
		      #map-canvas { height: 100%; }
			 
		    </style>
		    <script type="text/javascript"
		      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFa0ilx3XJk5BXYscBjpbq8LrssatDoGE&sensor=false">
		    </script>
		    <script type="text/javascript">
		      var center_lat = <c:out value="${center_lat}"/>
		      var center_lng = <c:out value="${center_lng}"/>
		      var places = <c:out value="${places}" escapeXml="false"/>;
		    </script>
		    <script type="text/javascript" src="map.js"></script>
		  </head>
		  <body>
		    <div id="side_bar">
		    
			    <p>Icon type:</p> 
			    <input type="radio" name="icon_type" id="rating" value="rating"  onclick="showRatings()">
			    	<label for="rating">Rating</label>  <br>
			    <input type="radio" name="icon_type" id="food" value="food"  onclick="showFoods()">
			    	<label for="food">Food</label>  <br>
			    <input type="radio" name="icon_type" id="price" value="price"  onclick="showPrices()">
			    	<label for="price">Price</label>  <br>
		    
		    
		    </div>
		    
		    <div id="map-canvas"></div>
		  </body>
        </c:otherwise>
    </c:choose>
</html>
