<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>WherEat ETL</title>
    <style type="text/css">
        table, th, td {border: 1px solid #9cf; border-collapse: collapse;}
        table {margin-bottom: 20px; padding: 5px}
    </style>  </head>
  <body onLoad="javascript:onPageLoad()">
    <script language="javascript">
        var cities = ["bcn", "mad", "val"];
        var idx = 0;
    
	    function handleReponse(city)
	    {
	          if (req.readyState == 4 && req.status == 200)
	          {
	              result = JSON.parse(req.responseText);
	              document.getElementById(city + "_gplaces").innerHTML = result.gplaces;
                  document.getElementById(city + "_tiplaces").innerHTML = result.tiplaces;
                  document.getElementById(city + "_yplaces").innerHTML = result.yplaces;
                  document.getElementById(city + "_total").innerHTML = result.total;
                  
                  if (idx < cities.length)
                  {
                      etlCity(cities[idx++]);
                  }
	          }
	    }
    
	    function etlCity(city)
	    {
            document.getElementById(city + "_gplaces").innerHTML = "LOADING...";
            document.getElementById(city + "_tiplaces").innerHTML = "LOADING...";
            document.getElementById(city + "_yplaces").innerHTML = "LOADING...";
            document.getElementById(city + "_total").innerHTML = "LOADING...";
            
	        if (window.XMLHttpRequest)
	        {// code for IE7+, Firefox, Chrome, Opera, Safari
	            req = new XMLHttpRequest();
	        }
	        else
	        {// code for IE6, IE5
	            req = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        
	        req.onreadystatechange = function(){handleReponse(city);};
	        var url = "/etl.do?city=" + city;
	        req.open("GET", url, true);
	        req.send();
	    }
	    
	    function onPageLoad()
	    {
            etlCity(cities[idx++]);
	    }
    </script>
    <table>
        <thead>
            <tr>
                <td>City</td>
	            <td>Google Places</td>
	            <td>TrippinIn</td>
	            <td>Yelp</td>
                <td>Total</td>
	        </tr>
        </thead>
        <tbody>
	        <tr>
	            <td>Barcelona</td>
	            <td id="bcn_gplaces">&nbsp;</td>
	            <td id="bcn_tiplaces">&nbsp;</td>
	            <td id="bcn_yplaces">&nbsp;</td>
	            <td id="bcn_total">&nbsp;</td>
	        </tr>
            <tr>
                <td>Madrid</td>
                <td id="mad_gplaces">&nbsp;</td>
                <td id="mad_tiplaces">&nbsp;</td>
                <td id="mad_yplaces">&nbsp;</td>
                <td id="mad_total">&nbsp;</td>
            </tr>
            <tr>
                <td>Valencia</td>
                <td id="val_gplaces">&nbsp;</td>
                <td id="val_tiplaces">&nbsp;</td>
                <td id="val_yplaces">&nbsp;</td>
                <td id="val_total">&nbsp;</td>
            </tr>
        </tbody>
    </table>
  </body>
</html>
