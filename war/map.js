/**
 * 
 */ 
  var map;
  
  var places;
  
  var currentMarkersType = 'rating';
  
  var infowindows = new Array();
  
  var markers =
  {
    rating: new Array(),
    food: new Array(),
    price: new Array()
  };
  
  var cityCoordinates =
  {
    bcn: {lat: 41.383, lng: 2.183},
    mad: {lat: 40.400, lng: -3.683},
    val: {lat: 39.466, lng: -0.383}
  };
  
  var places_url = '/restaurants.do';
  
  var req;

  google.maps.event.addDomListener(window, 'load', drawMap);
  
  function drawMap()
  {
	    var mapOptions = {
	      center: new google.maps.LatLng(cityCoordinates.mad.lat, cityCoordinates.mad.lng),
	      zoom: 6,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    
	    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	  }
 
  function handlePlacesReponse(city)
  {
		if (req.readyState == 4 && req.status == 200 && city != '...' )
		{
			map.setCenter(new google.maps.LatLng(cityCoordinates[city].lat, cityCoordinates[city].lng));
			map.setZoom(14);
			
			places = JSON.parse(req.responseText);
			closeInfoWindows();
			hideCurrentMarkers();
			createMarkers();
			showMarkers(currentMarkersType);
		}
  }
  
  function changeCity(city)
  {
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  req = new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		  req = new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  
	  req.onreadystatechange = function(){handlePlacesReponse(city);};
	  var url = places_url + '?city=' + city;
	  req.open("GET", url, true);
	  req.send();
  }
  
  function createInfoWindow(place)
  {
	  var contentString = '<div class="infoWindow">';
	  contentString += '<p>' + place.name + '</p>'; 
      if ('address' in place) contentString += place.address;
      if ('phone' in place) contentString += '<br/>' + place.phone;
      if ('website' in place) contentString += '<br/><a href=\"'+ place.website + '\" target=\"_other\">' + place.website + '</a>'; 
      contentString += '</div>';
	  
      google.maps.InfoWindow.prototype.isOpen = false;
      return new google.maps.InfoWindow({
	      content: contentString
	  });
  }
  
  function createMarkers()
  {
	  infowindows = new Array();
	  markers['rating'] = new Array();
	  markers['food'] = new Array();
	  markers['price'] = new Array();
	  
	  for (var i = 0; i < places.length; i++)
	  {	  
		  infowindows[i] = createInfoWindow(places[i]);
		  
		  markers['rating'][i] = createMarker(places[i], getRatingIcon(places[i].rating), infowindows[i]);
		  markers['food'][i] = createMarker(places[i], getFoodIcon(places[i].food), infowindows[i]);
		  markers['price'][i] = createMarker(places[i], getPriceIcon(places[i].price), infowindows[i]);
	  }	  
  }
  
  function createMarker(place, icon, infowindow)
  {
	  var marker = new google.maps.Marker({
		    position: new google.maps.LatLng(place.lat,place.lng),
		    map: map,
		    title: place.name,
		    visible: false,
		    icon: icon
		  });
	  
	  google.maps.event.addListener(marker, 'click', function() {
		    if (infowindow.isOpen)
		    {
		    	infowindow.close();
		    	infowindow.isOpen = false;
		    }
		    else
		    {
			    infowindow.open(map, marker);
			    infowindow.isOpen = true;
		    }
		  });
	  
	  return marker;
  }
  
  function getRatingIcon(rating)
  {
	  ratingDirectory = "ratingicons";
	  
	  ratingIcons = [
	      'yuck.png',
	      'meh.png',
	      'good.png',
	      'awesome.png'             
	  ];
	  
	  var icon = {
	    	url: ratingDirectory + "/" + ratingIcons[rating],
	    	scaledSize: new google.maps.Size(35,35)	  
	  }
	  return icon;
  }
  
  function getFoodIcon(food)
  {
	  foodDirectory = "foodicons";
	  
	  foodIcons = [
	      'chicken.png',
	      'fish.png',
	      'hamburger.png',
	      'hotdog.png',
	      'pizza.png',
	      'sandwitch.png',
	      'seafood.png',
	      'steak.png'
	  ];
	  
	  var icon = {
	    	url: foodDirectory + "/" + foodIcons[food],
	    	scaledSize: new google.maps.Size(35,35)	  
	  }
	  
	  return icon;
  }
  
  function getPriceIcon(price)
  {
	  priceDirectory = "priceicons";
	  
	  priceIcons = [
	      'cheap.png',
	      'medium.png',
	      'expensive.png',
	      'luxurious.png'
	  ];
	  
	  var icon = {
	    	url: priceDirectory + "/" + priceIcons[price],
	    	scaledSize: new google.maps.Size(35,35)	  
	  }
	  return icon;
  }
  
  function closeInfoWindows()
  {
	  for (var i = 0; i < infowindows.length; i++)
	  {
		  infowindows[i].close();
	  }	
  }
  
  function hideCurrentMarkers()
  {
	  for (var i = 0; i < markers[currentMarkersType].length; i++)
	  {
		  markers[currentMarkersType][i].setVisible(false);
	  }	
  }
  
  function showMarkers(newMarkersType)
  {
	  for (var i = 0; i < markers[newMarkersType].length; i++)
	  {
		  if (is_displayed(places[i]))
		  {
			  markers[newMarkersType][i].setVisible(true);
		  }
	  }
  }
  
  function changeIconsType(newMarkersType)
  {  
	  if (newMarkersType == currentMarkersType) return
	  
	  hideCurrentMarkers();
	  showMarkers(newMarkersType); 
	  currentMarkersType = newMarkersType;
  }
  
  function is_displayed(place)
  {
	  var ratingCheckboxes = 
	  [
		  'rating_one_star',
		  'rating_two_stars',
		  'rating_three_stars',
		  'rating_four_stars'
	  ]
	  
	  var priceCheckboxes = 
		  [
			  'price_less_10',
			  'price_10_20',
			  'price_20_50',
			  'price_more_50'
		  ]
	  
	  var foodCheckboxes = 
	  	  [
     	      'food_chicken',
     	      'food_fish',
     	      'food_hamburger',
     	      'food_hotdog',
     	      'food_pizza',
     	      'food_sandwitch',
     	      'food_seafood',
     	      'food_steak'
     	  ];
	  
	  if ( ! document.getElementById( ratingCheckboxes[place.rating] ).checked ) return false;
	  if ( ! document.getElementById( priceCheckboxes[place.price] ).checked ) return false;
	  if ( ! document.getElementById( foodCheckboxes[place.food] ).checked ) return false;
	  
	  return true;
  }
  
  function updateMarkers()
  {
	  for (var i = 0; i < markers[currentMarkersType].length; i++)
	  {
		  if (is_displayed(places[i]))
		  {
			  markers[currentMarkersType][i].setVisible(true);
		  }
		  else
		  {
			  markers[currentMarkersType][i].setVisible(false);
		  }
	  }
  }
  
  
  
