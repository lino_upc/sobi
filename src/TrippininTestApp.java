

import java.io.FileReader;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.core.UriBuilder;


import DAO.TrippininDetail;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class TrippininTestApp {
private static String BARCELONA_LATLONG = "41.38605,2.1700";
private static String BARCELONA_PLACE = "Hard%20Rock%20Cafe%20Barcelona";

public String getMatchURL(String apiKey)
{
    UriBuilder uriBuilder = UriBuilder.fromUri("http://api.v1.trippinin.com/match/");
    
    uriBuilder.path(BARCELONA_PLACE);

    uriBuilder.queryParam("coordinates", BARCELONA_LATLONG);
    uriBuilder.queryParam("key", apiKey);
    
    System.out.println("The URL is*******: " + uriBuilder.build().toString());
    
    return uriBuilder.build().toString();
}

public String getResolveURL(String apiKey)
{
    UriBuilder uriBuilder = UriBuilder.fromUri("http://api.v1.trippinin.com/resolve/");
    
    uriBuilder.path(BARCELONA_PLACE);

    uriBuilder.queryParam("coordinates", BARCELONA_LATLONG);
    uriBuilder.queryParam("key", apiKey);
    
    System.out.println("The URL is*******: " + uriBuilder.build().toString());
    
    return uriBuilder.build().toString();
}
    
public String getLocationBasedURL(String apiKey, String weekDay, Integer num)
{
	if (weekDay == "today")
	{ weekDay =this.weekDay();}
	
    UriBuilder uriBuilder = UriBuilder.fromUri("http://api.v1.trippinin.com/GeoSearch/");
    
    uriBuilder.path(BARCELONA_LATLONG);
    uriBuilder.path("eat");
    uriBuilder.path(weekDay);
    uriBuilder.path("evening");
    uriBuilder.queryParam("Radius", "5000");
    uriBuilder.queryParam("limit", num);
    uriBuilder.queryParam("offset", "0");
    uriBuilder.queryParam("key", apiKey);
    
    System.out.println("The URL is*******: " + uriBuilder.build().toString());

    return uriBuilder.build().toString();
}
    public String getAPIKey() throws Exception
    {
        // Read API key.
        Properties props = new Properties();
        props.load(new FileReader("properties/sobi.properties"));
        
        return props.getProperty("trippinin.api.key");
    }
    
  
    
    public String getTrippininID(String jsonResponse)
    {
    	JsonElement jsonObj = new JsonParser().parse(jsonResponse);
          
        String trippininId = "";
        if (jsonObj.isJsonObject())
        {
        	JsonObject  jobject = jsonObj.getAsJsonObject();
        	System.out.println("RESPONSE: " + jobject.get("status"));
            jobject = jobject.getAsJsonObject("response");
            JsonObject jarray = jobject.getAsJsonObject("data");
            trippininId = jarray.get("trippininid").getAsString();
            
            System.out.println("Trippinin id is : " + trippininId);
            System.out.println();
                

                    }
        return trippininId;
    }
   


    
    public static void getPlaceDetails(String jsonResponse)
    {
    	 JsonParser parser = new JsonParser();
         JsonObject obj = (JsonObject)parser.parse(jsonResponse);
         JsonObject response = obj.getAsJsonObject("response");
         JsonObject data = response.getAsJsonObject("data");
         String trippininId = data.get("trippininid").getAsString();
         System.out.println("trippinin Id - ---" + trippininId);
         
         Gson gson = new Gson();
        
         TrippininDetail obj2 = gson.fromJson(data, TrippininDetail.class);
         
         System.out.println(obj2);

    }
    
    
    public static void getLocationBased(String jsonResponse)
    {	
    	 JsonParser parser = new JsonParser();
    	 JsonObject jsonObj = (JsonObject)parser.parse(jsonResponse);
         System.out.println("RESPONSE: " + jsonObj.get("status"));
         JsonObject response = jsonObj.getAsJsonObject("response");
         JsonArray resultsArr = response.get("data").getAsJsonArray();

         for (JsonElement jse : resultsArr)
         {
         
             Gson gson = new Gson();
             
             TrippininDetail obj2 = gson.fromJson(jse, TrippininDetail.class);
             
             System.out.println(obj2);
         }
         

    }
    public String weekDay()
    {
    	         
        String weekDay = "";
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK); 
                
        switch (day) {
        case Calendar.SUNDAY:
        	weekDay ="sunday";
             break;
        case Calendar.MONDAY:
        	weekDay ="monday";
            break;
        case Calendar.TUESDAY:
        	weekDay ="tuesday";
             break;
        case Calendar.WEDNESDAY:
        	weekDay ="wednesday";
            break;
        case Calendar.THURSDAY:
        	weekDay ="thursday";
        case Calendar.FRIDAY:
        	weekDay ="friday";
             break;
        case Calendar.SATURDAY:
        	weekDay ="saturday";
            break;
    }
                  
        return weekDay ;
    }
    


    public static void main(String[] args) throws Exception
    {
    	System.out.println("Part 1 Mach" );
    	TrippininTestApp trippinin = new TrippininTestApp();
        
    	String apiKey = trippinin.getAPIKey();
    	
        String placesURLString = trippinin.getMatchURL(apiKey);
        System.out.println(placesURLString);
        
//        String jsonResponse =  WebUtils.getHTML(placesURLString);
        String jsonResponse = WebUtils.restGET(placesURLString);
       
        trippinin.getTrippininID(jsonResponse);
        
        System.out.println("" );
        System.out.println("Part 2 Resolve" );
        
        String resolveURLString = trippinin.getResolveURL(apiKey);
        System.out.println(resolveURLString);
        
//        String jsonResponse2 =  WebUtils.getHTML(resolveURLString);
        String jsonResponse2 = WebUtils.restGET(resolveURLString);
       
        TrippininTestApp.getPlaceDetails(jsonResponse2);
        
        System.out.println("" );
        System.out.println("Part 3 Place .." );
        
        String lbsveURLString = trippinin.getLocationBasedURL(apiKey, "saturday",10);
        System.out.println(lbsveURLString);
        
//        String jsonResponse3 =  WebUtils.getHTML(lbsveURLString);
        String jsonResponse3 = WebUtils.restGET(lbsveURLString);
       
        TrippininTestApp.getLocationBased(jsonResponse3);
        
        TrippininRetriever r = new TrippininRetriever();
        List<Place> places = r.retrieve(41.38605, 2.1700, 5000, "", apiKey, 10);
        for (Place pl : places)
        {
            System.out.println("Name: " + pl.getName());
            System.out.println("Lat: " + pl.getLat());
            System.out.println("Long: " + pl.getLng());
            System.out.println("Rating: " + pl.getRating());
            System.out.println();
        }
       
        
     }
}
