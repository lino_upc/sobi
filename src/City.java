
public class City
{
    public static City BARCELONA = new City("Barcelona", 41.38605, 2.183);
    public static City MADRID = new City("Madrid", 40.400, -3.6833);
    public static City VALENCIA = new City("Valencia", 39.4667, -0.3833);
    
    private String name;
    private double latitude;
    private double longitude;
    
    protected City(String name, double latitude, double longitude)
    {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName()
    {
        return name;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }
    
    public static City getCityByCode(String code)
    {
        if ("bcn".equals(code))
        {
            return City.BARCELONA;
        }
        else if ("mad".equals(code))
        {
            return City.MADRID;
        }
        else if ("val".equals(code))
        {
            return City.VALENCIA;
        }
        return null;
    }
}
