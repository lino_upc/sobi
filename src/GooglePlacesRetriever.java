import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.UriBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GooglePlacesRetriever implements PlaceRetriever
{
    @Override
    public List<Place> retrieve(double latitude, double longitude, int radius, String type, String apiKey, int maxResults) throws Exception
    {
        UriBuilder uriBuilder = UriBuilder.fromUri("https://maps.googleapis.com/maps/api/place/");
        uriBuilder.path("radarsearch").path("json");

        uriBuilder.queryParam("sensor", "false");
        uriBuilder.queryParam("location", Double.toString(latitude) + "," + Double.toString(longitude));
        uriBuilder.queryParam("radius", radius);
        uriBuilder.queryParam("types", type);
        uriBuilder.queryParam("key", apiKey);
        
        String requestURI = uriBuilder.build().toString();
        String jsonResponseStr = WebUtils.restGET(requestURI);
        JsonObject jsonResponse = WebUtils.toJsonObject(jsonResponseStr);
        String status = jsonResponse.get("status").getAsString();
        if (!"OK".equals(status))
        {
            throw new Exception("Call to Google Places returned " + status + " (URL was " + requestURI + ")");
        }
        
        JsonArray results = jsonResponse.get("results").getAsJsonArray();
        return extractPlaces(results, apiKey, maxResults);
    }   
    
    private List<Place> extractPlaces(JsonArray results, String apiKey, int maxResults) throws Exception
    {
        // Details about the JSON array returned by Google can be found here:
        // https://developers.google.com/places/documentation/details#PlaceDetailsResponses
        List<Place> places = new ArrayList<Place>(maxResults);
        for (JsonElement jse : results)
        {
            // Only retrieve the first MAX_RESULTS results.
            if (places.size() == maxResults)
            {
                break;
            }
            
            JsonObject jso = jse.getAsJsonObject();
            JsonElement referenceElem = jso.get("reference");

            // Make another call to get more details about this place.
            UriBuilder uriBuilder = UriBuilder.fromUri("https://maps.googleapis.com/maps/api/place/");
            uriBuilder.path("details").path("json");

            uriBuilder.queryParam("sensor", "false");
            uriBuilder.queryParam("key", apiKey);
            uriBuilder.queryParam("reference", referenceElem.getAsString());
            
            String detailsJsonString = WebUtils.restGET(uriBuilder.build().toString());
            JsonElement jsonEPlaceDetails = WebUtils.toJsonObject(detailsJsonString).get("result");
            if (jsonEPlaceDetails == null)
            {
                continue;
            }
            JsonObject jsonOPlaceDetails = jsonEPlaceDetails.getAsJsonObject();
            // Skip results that have no ratings.
            if (jsonOPlaceDetails.get("rating") == null)
            {
                continue;
            }
            
            String name = jsonOPlaceDetails.get("name").getAsString();
//            System.out.println(name);
            
            JsonObject jsonLocation = jsonOPlaceDetails.get("geometry").getAsJsonObject().get("location").getAsJsonObject();
            double latitude = jsonLocation.get("lat").getAsDouble();
            double longitude = jsonLocation.get("lng").getAsDouble();

            String address = WebUtils.getStringProperty(jsonOPlaceDetails, "formatted_address", null);
            String website = WebUtils.getStringProperty(jsonOPlaceDetails, "website", null);
            String phone = WebUtils.getStringProperty(jsonOPlaceDetails, "formatted_phone_number", null);
            
            double rawRating = jsonOPlaceDetails.get("rating").getAsDouble();
            int rating = normalizeRating(rawRating);
            
            int foodType = toFoodType(jsonOPlaceDetails.get("types"), name);
            int priceType = toPriceType(WebUtils.getIntProperty(jsonOPlaceDetails, "price_level", Place.PRICETYPE_CHEAP));
        
            Place p = new Place();
            p.setLat(latitude);
            p.setLng(longitude);
            p.setName(name);
            p.setRating(rating);
            p.setRawRating(rawRating);
            p.setFood(foodType);  
            p.setPrice(priceType);  
            p.setAddress(address);
            p.setWebsite(website);
            p.setPhone(phone);
            places.add(p);
        }
        
        return places;
    }
    
    private int toFoodType(JsonElement jsonETypes, String name)
    {
        if ((jsonETypes == null) || (!jsonETypes.isJsonArray()))
        {
            return Place.FOODTYPE_GENERIC;
        }
        JsonArray jsonATypes = jsonETypes.getAsJsonArray();
        
//        System.out.println(jsonATypes);
        int foodType = -1;
        for (JsonElement jsonEType : jsonATypes)
        {
            String type = jsonEType.getAsString();
            if ("cafe".equals(type))
            {
                foodType = Place.FOODTYPE_CAFE;
            }
        }
        
        // If we still don't have a known type, try to guess from the name.
        if (foodType == -1)
        {
            String nameLC = name.toLowerCase();
            if (nameLC.contains("pizza"))
            {
                foodType = Place.FOODTYPE_PIZZA;
            }
            if (nameLC.contains("burger"))
            {
                foodType = Place.FOODTYPE_FASTFOOD;
            }
        }
        
        if (foodType == -1)
        {
            foodType = Place.FOODTYPE_GENERIC;
        }
        
        return foodType;
    }
    
    private int toPriceType(int priceLevel)
    {
//        System.out.println("price level: " + priceLevel);
        switch (priceLevel)
        {
            case 0:
            case 1:
                return Place.PRICETYPE_CHEAP;
            case 2:
                return Place.PRICETYPE_MEDIUM;
            case 3:
                return Place.PRICETYPE_EXPENSIVE;
            case 4: 
                return Place.PRICETYPE_LUXURIOUS;
        }
        
        return Place.PRICETYPE_CHEAP;
    }
    
    private int normalizeRating(double rawRating)
    {
        int rating = (int) Math.round(rawRating) - 1;
        if (rating > 3)
        {
            rating = 3;
        }
        else if (rating < 0)
        {
            rating = 0;
        }
        return rating;
    }

    @Override
    public String getName()
    {
        return "Google Places";
    }
}
