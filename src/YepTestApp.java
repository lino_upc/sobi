import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;
import org.scribe.builder.ServiceBuilder;

import DAO.YelpDetail;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class YepTestApp {
	
		private static String BARCELONA_LAT = "41.38791700";
		private static String BARCELONA_LONG = "2.16991870";
		
		OAuthService service;
		Token accessToken;	

		
		 /**
		* Setup the Yelp API OAuth credentials.
		*
		* OAuth credentials are available from the developer site, under Manage API access (version 2 API).
		*
		*/
		    public void getYelpConnection() throws FileNotFoundException, IOException 
		    {
		        // Read API key.
		        Properties props = new Properties();
		        props.load(new FileReader("properties/sobi.properties"));
		        
		        String consumerKey = props.getProperty("yelp.consumer.key");
		        String consumerSecret = props.getProperty("yelp.consumer.secret");
		        String token = props.getProperty("yelp.token");
		        String tokenSecret = props.getProperty("yelp.token.secret");
		        
		        this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
		        this.accessToken = new Token(token, tokenSecret);
		    }
		    
		  
		    
		    /**
		    * Search with term and location.
		    *
		    * @param term Search term
		    * @param latitude Latitude
		    * @param longitude Longitude
		    * @return JSON string response
		    */
		      public String search(String term,String  limit, String  latitude, String  longitude, String radius, String yuwsid) {
		        OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/business_review_search");
		        request.addQuerystringParameter("term", term);
		        request.addQuerystringParameter("limit",  limit);
		        request.addQuerystringParameter("lat", latitude); 
		        request.addQuerystringParameter("long",  longitude);
		        request.addQuerystringParameter("radius",  radius);
		        request.addQuerystringParameter("ywsid",  yuwsid);
		        System.out.println("request: ..."+ request );
		        this.service.signRequest(this.accessToken, request);
		        Response response = request.send();
		        return response.getBody();
		      }

		      //
		      public String getYelpYuwsid() throws Exception
		      {
		          // Read API key.
		          Properties props = new Properties();
		          props.load(new FileReader("properties/sobi.properties"));
		          
		          return props.getProperty("yelp.api.1");
		      }
		      
		      
		      public static void getRestaurants(String jsonResponse)
		      {	
		      	 JsonParser parser = new JsonParser();
		      	 JsonObject jsonObj = (JsonObject)parser.parse(jsonResponse);
		           System.out.println("RESPONSE: " + jsonObj.get("status"));
		           JsonArray resultsArr = jsonObj.get("businesses").getAsJsonArray();

		           for (JsonElement jse : resultsArr)
		           {
		           
		               Gson gson = new Gson();
		               
		               YelpDetail obj2 = gson.fromJson(jse, YelpDetail.class);
		               
		               System.out.println(obj2);
		           }
		           
		      }
		      
		      
		      
		      private List<YelpDetail> getYepLocation(String jsonResponse)
		      {	
//		          System.out.println(jsonResponse);
		          JsonObject jsonObj = WebUtils.toJsonObject(jsonResponse);
		          
		           JsonArray resultsArr = jsonObj.getAsJsonObject().get("businesses").getAsJsonArray();
		           if (resultsArr.size() == 0)
		           {
		               return java.util.Collections.emptyList();
		           }

		           List<YelpDetail> places = new ArrayList<YelpDetail>(resultsArr.size());
		           for (JsonElement jse : resultsArr)
		           {
		               Gson gson = new Gson();
		               YelpDetail obj2 = gson.fromJson(jse, YelpDetail.class);
		               
//		               System.out.println(obj2.getTitle() + " MAIN:" + obj2.getMaincategory() + " SUB:" + obj2.getSubcategory());
		               
		               double rawRating = obj2.getAvg_rating();
		               int rating = normalizeRating(rawRating);
		               
		               // Determine food type from subcategory.
		               //int foodType = getFoodType(obj2.getSubcategory());
		               //places.add(new Place(obj2.getLatitude(), obj2.getLongitude(), obj2.getTitle(), rating, rawRating, foodType, Place.PRICETYPE_CHEAP));
		           }
		           
		           return places;
		      }
		      
		      private int normalizeRating(double rawRating)
		      {
		    	  int rating  = 0;
		          // Normalize ratings: from 5-1 -> 3-0
		          double ratingf = (rawRating -1.25);
		          if (ratingf  >= 3.75)
		              rating = 3;
		          if (ratingf  >= 2.5)
		              rating = 2;
		          if (ratingf  >= 1.25)
		              rating = 1;
		          if (ratingf  < 1.25)
		              rating = 0;
		          return rating;
		      }
		      
		      private int getFoodType(String subCategory)
		      {
		          int foodType = 0;
		          if ("Seafood Restaurant".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_SEAFOOD;
		          }
		          else if ("American Restaurant".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_AMERICAN;
		          }
		          else if ("Fast Food Restaurant".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_FASTFOOD;
		          }
		          else if ("Mediterranean Restaurant".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_MEDITERRANEAN;
		          }
		          else if ("Café".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_CAFE;
		          }
		          return foodType;
		      }
		      
		      
		      // CLI
		      public static void main(String[] args) throws Exception {
		        // Update tokens here from Yelp developers site, Manage API access.

		    	System.out.println("paso por aqui 1" );
		        YepTestApp yelp = new YepTestApp();
		        System.out.println("paso por aqui 2" );
		        yelp.getYelpConnection();
		        System.out.println("paso por aqui 3" );
		    //    String yuwsid = yelp.getYelpYuwsid();
		      //  String response = yelp.search("restaurante","20", BARCELONA_LAT , BARCELONA_LONG, "25",yuwsid);
		        //yelp.getRestaurants(response);

		        YelpRetriver r = new YelpRetriver();
		        List<Place> places = r.retrieve(41.38605, 2.1700, 5000, "", "", 10);
		        for (Place pl : places)
		        {
		            System.out.println("Name: " + pl.getName());
		            System.out.println("Lat: " + pl.getLat());
		            System.out.println("Long: " + pl.getLng());
		            System.out.println("Rating: " + pl.getRating());
		            System.out.println("Raw Rating: " + pl.getRawRating());
		            System.out.println("Category: " + pl.getFood());
		            System.out.println();
		        }
		       
		        
		      }
		    }