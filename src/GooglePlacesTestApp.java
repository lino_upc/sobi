import java.io.FileReader;
import java.util.List;
import java.util.Properties;

public class GooglePlacesTestApp
{
    private static double BARCELONA_LAT = 41.38791700;
    private static double BARCELONA_LONG = 2.16991870;
    private static int MAX_RESULTS = 10;
    
    public String getAPIKey() throws Exception
    {
        // Read API key.
        Properties props = new Properties();
        props.load(new FileReader("WebContent/WEB-INF/classes/sobi.properties"));
        
        return props.getProperty("google.places.api.key");
    }
        
    public void displayRadarSearchResult(List<Place> places, String apiKey) throws Exception
    {
        for (Place p : places)
        {                
            System.out.println("Name: " + p.getName());
            System.out.println("Rating: " + p.getRating());
            System.out.println("Lat: " + p.getLat());
            System.out.println("Long: " + p.getLng());
            System.out.println();
        }
    }

    public static void main(String[] args) throws Exception
    {
        GooglePlacesTestApp gp = new GooglePlacesTestApp();
        String apiKey = gp.getAPIKey();
        System.out.println("My key is " + apiKey);
        
        PlaceRetriever gpr = new GooglePlacesRetriever();
        List<Place> places = gpr.retrieve(BARCELONA_LAT, BARCELONA_LONG, 500, "food", apiKey, MAX_RESULTS);

        gp.displayRadarSearchResult(places, apiKey);
    }
}
