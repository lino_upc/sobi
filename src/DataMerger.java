import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;


public class DataMerger
{
    public List<Place> merge(List<Place> a1, List<Place> a2)
    {
        List<Place> output = new ArrayList<Place>();
        output.addAll(a1);
        int c = 0;
        // Go through the elements in the second array and see if there are any matches with elements from the first.
        for (Place p2 : a2)
        {
            boolean hasMatch = false;
            for (Place p1 : a1)
            {
                if (isLikelyMatch(p1, p2))
                {
                    // Merge the data.
                    hasMatch = true;
                    merge(p1, p2);
                    System.out.println("Merged object is");
                    System.out.println(p1);
                    c++;
                    break;
                }
            }
            
            if (!hasMatch)
            {
                // We consider this object to be unique.
                output.add(p2);
            }

        }
        
        System.out.println(c + " potential matches found (out of " + (a1.size() + a2.size()) + " items)");
        return output;
    }
    
    private void merge(Place p1, Place p2)
    {
        // Right now we only merge the rating data into the first Place (p1).
        // We also take any information on price and food type from p2 if p1 doesn't have it.
        // Otherwise all other information of the first object is maintained.
        int avgRating = Math.round((p1.getRating() + p2.getRating()) / 2);
        p1.setRating(avgRating);
        
        // For food type discrepancies, use p2's if p1 only has a "generic" type.
        if (p1.getFood() == Place.FOODTYPE_GENERIC)
        {
            p1.setFood(p2.getFood());
        }
        
        // For price type discrepancies, use the higher value.
        p1.setPrice(Math.max(p1.getPrice(), p2.getPrice()));
        
        // For name discrepancies, use the longer name (presumably it has more detail).
        if (p2.getName().length() > p1.getName().length())
        {
            p1.setName(p2.getName());
        }
    }
    
    private boolean isLikelyMatch(Place p1, Place p2)
    {
        // First we compare the latitude and longitude values.
        double latDiff = Math.abs(p1.getLat() - p2.getLat());
        double longDiff = Math.abs(p1.getLng() - p2.getLng());
        
        if ((latDiff <= 0.001) && (longDiff <= 0.001))
        {
//            System.out.println("Lat/long diff within threshold:");
//            System.out.println(o1);
//            System.out.println(o2);
//            System.out.println("lat difference: " + latDiff);
//            System.out.println("lng difference: " + longDiff);
            
            if (isNameMatch(p1, p2))
            {
                System.out.println("Name check returns a positive match");
                System.out.println(p1);
                System.out.println(p2);
                // Consider these two objects to be the same.
                return true;
            }
        }
        return false;
    }
    
    private boolean isNameMatch(Place p1, Place p2)
    {
        String n1 = p1.getName().toLowerCase();
        String n2 = p2.getName().toLowerCase();
        
        // Check if a case-insensitive compare returns a match.
        if (n1.equals(n2))
        {
            return true;
        }
        
        // Check if the first n characters of the strings are a match.
        int shorter = Math.min(n1.length(), n2.length());
        n1 = n1.substring(0, shorter);
        n2 = n2.substring(0, shorter);
        if (n1.equals(n2))
        {
            return true;
        }
        
        // Finally, find out how many characters differ between the two strings (Hamming distance).
        // Note that at this point the strings are of equal length.
        int diffChars = 0;
        for (int i = 0; i < n1.length(); i++)
        {
            if (n1.charAt(i) != n2.charAt(i))
            {
                diffChars++;
            }
        }
        
        // If the number of different characters is below a certain threshold, 
        // we consider the two strings "close enough".
        return (diffChars <= 2);
    }
        
    public static void main(String[] args) throws Exception
    {
        List<Place> gdata = readTestData("gdata.txt");
        List<Place> tidata = readTestData("tidata.txt");
        List<Place> ydata = readTestData("ydata.txt");
//        System.out.println(gdata.size());
//        System.out.println(tidata.size());
//        System.out.println(ydata.size());
        
        DataMerger d = new DataMerger();
        List<Place> merge1 = d.merge(gdata, tidata);
        System.out.println("Merged list has " + merge1.size());
        List<Place> merge2 = d.merge(merge1, ydata);
        System.out.println("Merged list has " + merge2.size());
    }
    
    private static List<Place> readTestData(String filename) throws Exception
    {
        BufferedReader gr = new BufferedReader(new InputStreamReader(DataMerger.class.getResourceAsStream("/" + filename)));
        
        StringBuilder sb = new StringBuilder();
        String s = gr.readLine();
        while (s != null)
        {
            sb.append(s);
            s = gr.readLine();
        }

        List<Place> places = new ArrayList<Place>();
        JsonArray arr = WebUtils.toJsonArray(sb.toString());
        for (JsonElement e : arr)
        {
            places.add(WebUtils.toPlace(e.getAsJsonObject()));
        }
        return places;
    }
}
