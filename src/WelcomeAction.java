import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DAO.GAEDatastoreService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


public class WelcomeAction extends Action
{
    private static double BARCELONA_LAT = 41.38605;
    private static double BARCELONA_LONG = 2.17;
    private static int DEFAULT_MAX_RESULTS = 45;    

    @Override
    public ActionForward execute(ActionMapping mapping, 
                                 ActionForm form,
                                 HttpServletRequest request, 
                                 HttpServletResponse response)
            throws Exception
    {
        JsonArray jsonPlaces = new JsonArray();
        
        if (request.getParameter("realtime") != null)
        {
            // Retrieve and display realtime data (just like before).
            Properties props = new Properties();
            props.load(getClass().getResourceAsStream("/sobi.properties"));
            
            int max = DEFAULT_MAX_RESULTS;
            try
            {
                max = Integer.parseInt(request.getParameter("max"));
            }
            catch (Exception e)
            {
                max = DEFAULT_MAX_RESULTS;
            }

            String apiKey;
            PlaceRetriever retriever;
            int radius;
            if ("google".equals(request.getParameter("source")))
            {
                retriever = new GooglePlacesRetriever();
                apiKey = props.getProperty("google.places.api.key");
                radius = 300;
            }
            else if ("yelp".equals(request.getParameter("source")))
            {
                retriever = new GooglePlacesRetriever();
                apiKey = props.getProperty("yelp.api.1");
                radius = 25;
            }
            else
            {
                retriever = new TrippininRetriever();
                apiKey = props.getProperty("trippinin.api.key");            
                radius = 5000;
            }
            
            List<Place> places = retriever.retrieve(BARCELONA_LAT, BARCELONA_LONG, radius, "food", apiKey, max);
            
            for (Place p : places)
            {
                jsonPlaces.add(WebUtils.toJsonPlace(p));
            }
        }
        else
        {
            // Load data from datastore.
            jsonPlaces = loadData();
        }
        
        request.setAttribute("center_lat", BARCELONA_LAT);
        request.setAttribute("center_lng", BARCELONA_LONG);
        
        if (request.getParameter("raw") != null)
        {
            // Set up pretty printing of data.
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            JsonElement je = new JsonParser().parse(jsonPlaces.toString());
            request.setAttribute("places", gson.toJson(je));
            request.setAttribute("showRawData", Boolean.TRUE);
        }
        else
        {
            request.setAttribute("places", jsonPlaces);
        }
        
        return mapping.findForward("success");
    }
    
    private JsonArray loadData() throws Exception
    {
        JsonArray jsonPlaces = new JsonArray();
        String jsonPlacesString = GAEDatastoreService.get("Location", "Barcelona");
        if (jsonPlacesString == null)
        {
            System.err.println("GAE Datastore returned no data!");
        }
        else
        {
            jsonPlaces = WebUtils.toJsonArray(jsonPlacesString);
            System.out.println("Retrieved " + jsonPlaces.size() + " places from datastore");
        }
        return jsonPlaces;
    }
}
