import java.util.List;

public interface PlaceRetriever
{
    public String getName();

    public List<Place> retrieve(double latitude, double longitude, int radius,
            String type, String apiKey, int maxResults) throws Exception;

}