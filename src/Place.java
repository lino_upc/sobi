
public class Place
{
    private double lat;
    private double lng;
    private String name;
    private int rating;
    private double rawRating;
    private int food; // 0 = chicken.png, 1 = fish.png, 2 = hamburger.png, 3 = hotdog.png, 4 = pizza.png, 
                          // 5 = sandwitch.png 6 = seafood.png, 7 = steak.png'
    private int price; // 0 = cheap.png, 1 = medium.png, 2 = expensive.png, 3 = luxurious.png
    private String address;
    private String website;
    private String phone;

    public static int FOODTYPE_GENERIC = 0;
    public static int FOODTYPE_MEDITERRANEAN = 1;
    public static int FOODTYPE_FASTFOOD = 2;
    public static int FOODTYPE_AMERICAN = 3;
    public static int FOODTYPE_PIZZA = 4;
    public static int FOODTYPE_CAFE = 5;
    public static int FOODTYPE_SEAFOOD = 6;
    public static int FOODTYPE_STEAKHOUSE = 7;

    public static int PRICETYPE_CHEAP = 0;
    public static int PRICETYPE_MEDIUM = 1;
    public static int PRICETYPE_EXPENSIVE = 2;
    public static int PRICETYPE_LUXURIOUS = 3;
    
    public Place()
    {
    }

    public double getLat()
    {
        return lat;
    }

    public double getLng()
    {
        return lng;
    }

    public String getName()
    {
        return name;
    }

    public int getRating()
    {
        return rating;
    }

    public double getRawRating()
    {
        return rawRating;
    }

    public int getFood()
    {
        return food;
    }
    
    public int getPrice()
    {
        return price;
    }

    public void setLat(double latitude)
    {
        this.lat = latitude;
    }

    public void setLng(double longitude)
    {
        this.lng = longitude;
    }

    public void setFood(int foodType)
    {
        this.food = foodType;
    }

    public void setPrice(int priceType)
    {
        this.price = priceType;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRating(int rating)
    {
        this.rating = rating;
    }

    public void setRawRating(double rawRating)
    {
        this.rawRating = rawRating;
    }

    @Override
    public String toString()
    {
        return WebUtils.toJsonPlace(this).toString();
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setWebsite(String website)
    {
        this.website = website;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }
}
