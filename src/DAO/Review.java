package DAO;

public class Review {


	private Double rating;
    private String text_excerpt;
	private String user_name;
	
	    // Getters and setters are not required for this example.
	    // GSON sets the fields directly using reflection.


	
	    @Override
	    public String toString(){
	         StringBuilder sb = new StringBuilder();
	         sb.append("rating: " + rating +  "\n");
	         sb.append("text_excerpt: " + text_excerpt + "\n");
	         sb.append("user_name: " + user_name +  "\n");
	         return sb.toString();
	    }
	
}
