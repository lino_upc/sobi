package DAO;

import java.util.List;

public class TrippininDetail {

	
	 private String trippininid;
	 private String title;
	 private String maincategory;   
	 private String subcategory;
	 private Double latitude;
	 private Double longitude;
	 private Integer totalsocialgestures; 
	 private Integer totalcheckins; 
	 private Integer totalwerehere; 
	 private Integer totallikes; 
	 private Double rating;
	 private List<SocialRankings> socialrankings ;
	 

	 

	public String getTrippininid()
    {
        return trippininid;
    }




    public String getTitle()
    {
        return title;
    }




    public String getMaincategory()
    {
        return maincategory;
    }




    public String getSubcategory()
    {
        return subcategory;
    }




    public Double getLatitude()
    {
        return latitude;
    }




    public Double getLongitude()
    {
        return longitude;
    }




    public Integer getTotalsocialgestures()
    {
        return totalsocialgestures;
    }




    public Integer getTotalcheckins()
    {
        return totalcheckins;
    }




    public Integer getTotalwerehere()
    {
        return totalwerehere;
    }




    public Integer getTotallikes()
    {
        return totallikes;
    }




    public Double getRating()
    {
        return rating;
    }




    public List<SocialRankings> getSocialrankings()
    {
        return socialrankings;
    }




        @Override
	    public String toString(){
	         StringBuilder sb = new StringBuilder();
	         sb.append("trippininid: " +trippininid +  "\n");
	         sb.append("title: " + title + "\n");
	         sb.append("maincategory: " + maincategory + "\n");
	         sb.append("subcategory: " + subcategory + "\n");
	         sb.append("latitude: " + latitude + "\n");
	         sb.append("longitude: " + longitude + "\n");
	         sb.append("totalsocialgestures: " + totalsocialgestures + "\n");
	         sb.append("totalcheckins: " + totalcheckins + "\n");
	         sb.append("totalwerehere: " + totalwerehere + "\n");
	         sb.append("totallikes: " + totallikes + "\n");
	         sb.append("rating: " + rating + "\n");
	         sb.append("socialrankings: " + socialrankings + "\n");
	         return sb.toString();
	    }
}
