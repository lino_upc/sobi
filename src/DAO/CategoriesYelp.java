package DAO;

public class CategoriesYelp {

	private String category_filter ;
	private String name;
	private String search_url;

	
	    // Getters and setters are not required for this example.
	    // GSON sets the fields directly using reflection.

	public String getCategory_filter()
    {
        return category_filter;
    }

	public String getName()
    {
        return name;
    }


    public String getSearch_url()
    {
        return search_url;
    }


	
	    @Override
	    public String toString(){
	         StringBuilder sb = new StringBuilder();
	         sb.append("category_filter: " + category_filter +  "\n");
	         sb.append("name: " + name + "\n");
	         sb.append("search_url: " + search_url + "\n");
	         return sb.toString();
	    }
}
