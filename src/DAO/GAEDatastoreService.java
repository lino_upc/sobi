package DAO;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

public class GAEDatastoreService
{
    public static void put(String kind, String name, Object o)
    {
        Entity entity = new Entity(kind, name);
        
        if ((o instanceof String) && (((String)o).length() > 500))
        {
            o = new Text((String)o);
        }
        entity.setProperty("data", o);
        
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(entity);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> T get(String kind, String name)
    {
        Key key = KeyFactory.createKey(kind, name);
        
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = null;
        try
        {
            entity = datastore.get(key);
        } 
        catch (EntityNotFoundException e)
        {
            e.printStackTrace();
            return (T)null;
        }
        
        Object o = entity.getProperty("data");
        if (o instanceof Text)
        {
            o = ((Text)o).getValue();
        }
        return (T)o;
    }
}
