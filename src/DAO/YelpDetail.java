package DAO;

import java.util.List;

public class YelpDetail {


	 private String address1;
	 private List<CategoriesYelp> categories;
	 private String name;
	 private String phone;
	 private String state;
	 private String city;
	 private String country;
	 private Double avg_rating ;
	 private Integer review_count;
	 private List<Review> reviews;
	 private String url;

	 

	public String getAddress1()
   {
       return address1;
   }

	public List<CategoriesYelp> getCategories()
	{
	     return categories;
	}

   public String getName()
   {
       return name;
   }

   public String getPhone()
   {
       return phone;
   }

   public String getState()
   {
       return state;
   }

   public String getCountry()
   {
       return country;
   }

   public String getCity()
   {
       return city;
   }
   
   public Double getAvg_rating()
   {
       return avg_rating;
   }

   public Integer getReview_count()
   {
       return review_count;
   }


   public List<Review> getReviews()
   {
       return reviews;
   }


	 
       @Override
	    public String toString(){
	         StringBuilder sb = new StringBuilder();
	         sb.append("address1: " + address1 +  "\n");
	         sb.append("name: " + name + "\n");
	         sb.append("categories: " + categories + "\n");
	         sb.append("phone: " + phone + "\n");
	         sb.append("state: " + state + "\n");
	         sb.append("city: " + city + "\n");
	         sb.append("country: " + country + "\n");
	         sb.append("avg_rating: " + avg_rating + "\n");
	         sb.append("review_count: " + review_count + "\n");
	         sb.append(" reviews: " +  reviews + "\n");
	         return sb.toString();
	    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}
