import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DAO.GAEDatastoreService;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ETLAction extends Action
{
    @Override
    public ActionForward execute(ActionMapping mapping, 
                                 ActionForm form,
                                 HttpServletRequest request, 
                                 HttpServletResponse response)
            throws Exception
    {
        String cityCode = request.getParameter("city");
        if (cityCode == null)
        {
            return mapping.findForward("etlStart");
        }
        
        City city = City.getCityByCode(cityCode);
        JsonObject jsonOResults = retrievePlacesFromSources(city);
        request.setAttribute("results", jsonOResults);
        return mapping.findForward("etlResult");
    }
    
    private JsonObject retrievePlacesFromSources(City city) throws Exception
    {
        if (city == null)
        {
            JsonObject jsonOResults = new JsonObject();
            jsonOResults.addProperty("gplaces", 0);
            jsonOResults.addProperty("tiplaces", 0);
            jsonOResults.addProperty("yplaces", 0);
            jsonOResults.addProperty("total", 0);
            return jsonOResults;
        }
        
        Properties props = new Properties();
        props.load(getClass().getResourceAsStream("/sobi.properties"));
        
        // Retrieve Google Places data.
        List<Place> gPlaces = retrievePlaces(city, new GooglePlacesRetriever(), props.getProperty("google.places.api.key"), 500, 150);
//        List<Place> gPlaces = Collections.emptyList();
        
        // Retrieve TrippinIn data.
        List<Place> tiPlaces = retrievePlaces(city, new TrippininRetriever(), props.getProperty("trippinin.api.key"), 5000, 200);
//        List<Place> tiPlaces = Collections.emptyList();

        // Retrieve Yelp data.
        List<Place> yPlaces = retrievePlaces(city, new YelpRetriver(), props.getProperty("l9-W9gcfn6PGbvyJHqHylw"), 25, 200);
//        List<Place> yPlaces = Collections.emptyList();
        
        DataMerger merger = new DataMerger();
        List<Place> places = merger.merge(gPlaces, tiPlaces);
        places = merger.merge(places, yPlaces);
        
        // Convert the list of places into a Json array.
        JsonArray jsonPlaces = new JsonArray();
        for (Place p : places)
        {
            jsonPlaces.add(WebUtils.toJsonPlace(p));
        }
        
        // Store data in the Datastore.
        // TODO before overwriting existing data, read first, convert to array, then reconcile against retrieved list.
        // Note that this requires that we have a way to identify Places inside the application, otherwise we'd have to do the 
        // lat/long + name resolution all over again.
        GAEDatastoreService.put("Location", city.getName(), jsonPlaces.toString());
        System.out.println("Stored " + jsonPlaces.size() + " places in datastore after merging");

        JsonObject jsonOResults = new JsonObject();
        jsonOResults.addProperty("gplaces", gPlaces.size());
        jsonOResults.addProperty("tiplaces", tiPlaces.size());
        jsonOResults.addProperty("yplaces", yPlaces.size());
        jsonOResults.addProperty("total", jsonPlaces.size());
        return jsonOResults;
    }
    
    private List<Place> retrievePlaces(City city, PlaceRetriever retriever, String apiKey, int radius, int max) throws Exception
    {
        List<Place> places = Collections.emptyList();
        try
        {
            long start = System.currentTimeMillis();
            places = retriever.retrieve(city.getLatitude(), city.getLongitude(), radius, "food", apiKey, max);
            long end = System.currentTimeMillis();
            System.out.println("Retrieving " + places.size() + " items from "+ retriever.getName() + " took " + (end - start) + "ms");
        }
        catch (Exception e)
        {
            System.err.println("Exception while trying to retrieve data from " + retriever.getName());
            e.printStackTrace();
        }
        return places;
    }
}
