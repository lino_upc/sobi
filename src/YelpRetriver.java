import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import DAO.CategoriesYelp;
import DAO.Location;
import DAO.YelpDetail;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


public class YelpRetriver  implements PlaceRetriever
{
	
	OAuthService service;
	Token accessToken;	
	
    @Override
    public List<Place> retrieve(double latitude, double longitude, int radius,
                                String type, String apiKey, int maxResults) throws Exception
    {
        getYelpConnection();
        
        String yuwsid = getYelpYuwsid();
        
        String latitudeS= Double.toString(latitude);
        String longitudeS=Double.toString(longitude);
        
        String response = search("restaurante","100", latitudeS , longitudeS, "25",yuwsid);
      
        return getPlaces(response);
     }

    
  
    
    private List<Place> getPlaces(String jsonResponse)
    {	
//        System.out.println(jsonResponse);
        JsonObject jsonObj = WebUtils.toJsonObject(jsonResponse);
        
         JsonArray resultsArr = jsonObj.getAsJsonObject().get("businesses").getAsJsonArray();
         if (resultsArr.size() == 0)
         {
             return java.util.Collections.emptyList();
         }

         List<Place> places = new ArrayList<Place>(resultsArr.size());
         for (JsonElement jse : resultsArr)
         {
             Gson gson = new Gson();
             YelpDetail obj2 = gson.fromJson(jse, YelpDetail.class);
             
//             System.out.println(obj2.getTitle() + " MAIN:" + obj2.getMaincategory() + " SUB:" + obj2.getSubcategory());
             
             double rawRating = obj2.getAvg_rating();
             int rating = normalizeRating(rawRating);
             
             // Determine food type from subcategory
             List<CategoriesYelp> category = obj2.getCategories();
             
             Location loca=getLatLong(obj2.getAddress1(),obj2.getCity(),obj2.getCountry());
             
             double lat = 0;
             double longi =0;
             
//             System.out.println("Location is ...: " + loca);
             
             if (loca.getLat() != null && loca.getLong()!= null){
//            	 System.out.println("lat is 1 ...: " + loca.getLat());
            	 lat = Double.parseDouble(loca.getLat());
//            	 System.out.println("lat is ...: " + lat);
            	 
//            	 System.out.println("longi is 1...: " + loca.getLong());
             	 longi = Double.parseDouble(loca.getLong());
//             	 System.out.println("longi is ...: " + longi);
             }
             int foodType = getFoodType(category);
             Place p = new Place();
             p.setLat(lat);
             p.setLng(longi);
             p.setName(obj2.getName());
             p.setRating(rating);
             p.setRawRating(rawRating);
             p.setFood(foodType);
             p.setPrice(Place.PRICETYPE_CHEAP); // unfortunately no price information available
             p.setAddress(obj2.getAddress1());
             p.setWebsite(obj2.getUrl());
             p.setPhone(obj2.getPhone());
             places.add(p);
//             System.out.println(p);
         }    
         
         return places;
    }
    
    /**
	* Setup the Yelp API OAuth credentials.
	*
	* OAuth credentials are available from the developer site, under Manage API access (version 2 API).
	*
	*/
	    public void getYelpConnection() throws FileNotFoundException, IOException 
	    {
	        // Read API key.
	      /*  Properties props = new Properties();
	        props.load(new FileReader("properties/sobi.properties"));
	        
	        String consumerKey = props.getProperty("yelp.consumer.key");
	        String consumerSecret = props.getProperty("yelp.consumer.secret");
	        String token = props.getProperty("yelp.token");
	        String tokenSecret = props.getProperty("yelp.token.secret");
	        */
	    	String consumerKey = "Ve4URuOKxcP6Ce3KnOa-4g";
	        String consumerSecret = "aXNXwOb_uXlTpzgdebeYsnapVt8";
	        String token = "2pv-oEJi8l1rk8g4hiUZL22OocdyZ3qb";
	        String tokenSecret = "7QX8PTA7DkNYc0bbDt9iJiRZ7vc";
	    	

	        this.service = new ServiceBuilder().provider(YelpApi2.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
	        this.accessToken = new Token(token, tokenSecret);
	    }
	    
	  
	    
	    /**
	    * Search with term and location.
	    *
	    * @param term Search term
	    * @param latitude Latitude
	    * @param longitude Longitude
	    * @return JSON string response
	    */
	      public String search(String term,String  limit, String  latitude, String  longitude, String radius, String yuwsid) {
	        OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/business_review_search");
	        request.addQuerystringParameter("term", term);
	        request.addQuerystringParameter("limit",  limit);
	        request.addQuerystringParameter("lat", latitude); 
	        request.addQuerystringParameter("long",  longitude);
	        request.addQuerystringParameter("radius",  radius);
	        request.addQuerystringParameter("ywsid",  yuwsid);
//	        System.out.println("request: ..."+ request );
	        this.service.signRequest(this.accessToken, request);
	        Response response = request.send();
	        return response.getBody();
	      }

	      //
	      public String getYelpYuwsid() throws Exception
	      {
	          // Read API key.
	         /* Properties props = new Properties();
	          props.load(new FileReader("properties/sobi.properties"));
	          
	          return props.getProperty("yelp.api.1");*/
	    	  return "l9-W9gcfn6PGbvyJHqHylw";
	      }
	      	      

	      
	      private Location getLatLong( String address, String city, String country){
	    	  
	    	  String addressF = address + "," + city + "," + country;
	    	  Location loca = GoogleGeoCode.getLatLong(addressF);
	    	  return loca;
	      }
	    
	      
	      private int normalizeRating(double rawRating)
	      {
	    	  int rating  = 0;
	          // Normalize ratings: from 5-1 -> 3-0
	          double ratingf = (rawRating);
	          if (ratingf  >= 3.75)
	              rating = 3;
	          if (ratingf  < 3.75 && ratingf  >= 2.5)
	              rating = 2;
	          if (ratingf  < 2.5 && ratingf  >= 1.25)
	              rating = 1;
	          if (ratingf  < 1.25)
	              rating = 0;
	          return rating;
	      }
	      
	      private int getFoodType(List<CategoriesYelp> category)
	      {
	    	  int foodType = 0;
	    	  
	    	  if (category.size() > 0){
	    		 CategoriesYelp categoryA = (category.get(0));
	    		 String subCategory=categoryA.getName();
   
		          if ("seafood".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_SEAFOOD;
		          }
		          else if ("american".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_AMERICAN;
		          }
		          else if ("Fast Food Restaurant".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_FASTFOOD;
		          }
		          else if ("mediterranean".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_MEDITERRANEAN;
		          }
		          else if ("Café".equalsIgnoreCase(subCategory))
		          {
		              foodType = Place.FOODTYPE_CAFE;
		          }
	    	  }else{
	    		  foodType = Place.FOODTYPE_CAFE;
	    	  }
	          return foodType;
	      }


    @Override
    public String getName()
    {
        return "yelp";
    }

}
