import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class WebUtils
{
    public static JsonObject toJsonObject(String jsonString)
    {
        try
        {
            JsonElement jsonElm = new JsonParser().parse(jsonString);
            if (jsonElm.isJsonNull())
            {
                return new JsonObject();
            }
                
            return jsonElm.getAsJsonObject();
        }
        catch (Exception e)
        {
            System.err.println(e);
            return new JsonObject();
        }
    }
    
    public static JsonArray toJsonArray(String jsonString)
    {
        try
        {
            JsonElement jsonElm = new JsonParser().parse(jsonString);
            if (jsonElm.isJsonNull())
            {
                return new JsonArray();
            }
            return jsonElm.getAsJsonArray();
        }
        catch (Exception e)
        {
            System.err.println(e);
            return new JsonArray();
        }
    }
    
    public static JsonObject toJsonPlace(Place pl)
    {
        return new Gson().toJsonTree(pl).getAsJsonObject();
    }

    public static Place toPlace(JsonObject o)
    {
        return new Gson().fromJson(o, Place.class);
    }
    
    // A way to safely get a String property from a JsonObject.
    public static String getStringProperty(JsonObject o, String name, String defaultVal)
    {
        JsonElement e = o.get(name);
        if ((e == null) || (e.isJsonNull()))
        {
            return defaultVal;
        }
        return e.getAsString();
    }
    
    // A way to safely get an integer property from a JsonObject.
    public static int getIntProperty(JsonObject o, String name, int defaultVal)
    {
        JsonElement e = o.get(name);
        if ((e == null) || (e.isJsonNull()))
        {
            return defaultVal;
        }
        return e.getAsInt();
    }
    
    public static String restGET(String URLString) throws Exception
    {
        URL url = new URL(URLString);
        URLConnection conn = url.openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder output = new StringBuilder();
        String line = reader.readLine();
        while (line != null)
        {
            output.append(line);
            line = reader.readLine();
        }
        return output.toString();
    }
}
