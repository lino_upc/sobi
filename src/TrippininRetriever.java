

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.UriBuilder;

import DAO.TrippininDetail;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


public class TrippininRetriever implements PlaceRetriever
{
    @Override
    public List<Place> retrieve(double latitude, double longitude, int radius,
                                String type, String apiKey, int maxResults) throws Exception
    {
        String lbsveURLString = getLocationBasedURL(latitude, longitude, apiKey, "saturday", radius, maxResults);
        
//        System.out.println(lbsveURLString);
        
        String jsonResponse3 = WebUtils.restGET(lbsveURLString);
       
        return getLocationBased(jsonResponse3);
     }

    
    private String getLocationBasedURL(double latitude, double longitude, String apiKey, String weekDay, int radius, int num)
    {
        UriBuilder uriBuilder = UriBuilder.fromUri("http://api.v1.trippinin.com/GeoSearch/");
        
        uriBuilder.path(Double.toString(latitude) + "," + Double.toString(longitude));
        uriBuilder.path("eat");
        uriBuilder.path(weekDay);
        uriBuilder.path("evening");
        uriBuilder.queryParam("Radius", radius);
        uriBuilder.queryParam("limit", num);
        uriBuilder.queryParam("offset", "0");
        uriBuilder.queryParam("key", apiKey);
        
        return uriBuilder.build().toString();
    }
    
    private List<Place> getLocationBased(String jsonResponse)
    {	
//        System.out.println(jsonResponse);
        JsonObject jsonObj = WebUtils.toJsonObject(jsonResponse);
        
    	 JsonElement response = jsonObj.get("response");
    	 if (response.isJsonArray() && response.getAsJsonArray().size() == 0)
    	 {
    	     return java.util.Collections.emptyList();
    	 }

         JsonArray resultsArr = response.getAsJsonObject().get("data").getAsJsonArray();
         if (resultsArr.size() == 0)
         {
             return java.util.Collections.emptyList();
         }

         List<Place> places = new ArrayList<Place>(resultsArr.size());
         for (JsonElement jse : resultsArr)
         {
             Gson gson = new Gson();
             TrippininDetail obj2 = gson.fromJson(jse, TrippininDetail.class);
             
//             System.out.println(obj2.getTitle() + " MAIN:" + obj2.getMaincategory() + " SUB:" + obj2.getSubcategory());
             
             double rawRating = obj2.getRating();
             int rating = normalizeRating(rawRating);
             
             // Determine food type from subcategory.
             int foodType = getFoodType(obj2.getSubcategory());
             Place p = new Place();
             p.setLat(obj2.getLatitude());
             p.setLng(obj2.getLongitude());
             p.setName(obj2.getTitle());
             p.setRating(rating);
             p.setRawRating(rawRating);
             p.setFood(foodType);
             p.setPrice(Place.PRICETYPE_CHEAP); // unfortunately no price information available
             places.add(p);
         }
         
         return places;
    }
    
    private int normalizeRating(double rawRating)
    {
        // Normalize ratings: from 10-1 -> 3-0
        int rating = ((int)Math.round(rawRating) / 2) - 1;
        if (rating > 3)
            rating = 3;
        if (rating < 0)
            rating = 0;
        return rating;
    }
    
    private int getFoodType(String subCategory)
    {
        int foodType = 0;
        if ("Seafood Restaurant".equalsIgnoreCase(subCategory))
        {
            foodType = Place.FOODTYPE_SEAFOOD;
        }
        else if ("American Restaurant".equalsIgnoreCase(subCategory))
        {
            foodType = Place.FOODTYPE_AMERICAN;
        }
        else if ("Fast Food Restaurant".equalsIgnoreCase(subCategory))
        {
            foodType = Place.FOODTYPE_FASTFOOD;
        }
        else if ("Mediterranean Restaurant".equalsIgnoreCase(subCategory))
        {
            foodType = Place.FOODTYPE_MEDITERRANEAN;
        }
        else if ("Café".equalsIgnoreCase(subCategory))
        {
            foodType = Place.FOODTYPE_CAFE;
        }
        return foodType;
    }


    @Override
    public String getName()
    {
        return "TrippinIn";
    }
}
