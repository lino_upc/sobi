import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DAO.GAEDatastoreService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


public class GetRestaurantsAction extends Action
{
    @Override
    public ActionForward execute(ActionMapping mapping, 
                                 ActionForm form,
                                 HttpServletRequest request, 
                                 HttpServletResponse response)
            throws Exception
    {
        String cityCode = request.getParameter("city");
        if (cityCode == null)
        {
            cityCode = "bcn";
        }
        City city = City.getCityByCode(cityCode);
        JsonArray jsonPlaces = loadData(city);
        
        // Set up pretty printing of data.
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement je = new JsonParser().parse(jsonPlaces.toString());
        request.setAttribute("places", gson.toJson(je));
        
        return mapping.findForward("success");
    }
    
    private JsonArray loadData(City city) throws Exception
    {
        JsonArray jsonPlaces = new JsonArray();
        String jsonPlacesString = GAEDatastoreService.get("Location", city.getName());
        if (jsonPlacesString == null)
        {
            System.err.println("GAE Datastore returned no data!");
        }
        else
        {
            jsonPlaces = WebUtils.toJsonArray(jsonPlacesString);
            System.out.println("Retrieved " + jsonPlaces.size() + " places from datastore");
        }
        return jsonPlaces;
    }
}
